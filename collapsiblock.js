(function($) {

  Drupal.Collapsiblock = Drupal.Collapsiblock || {};

  Drupal.Collapsiblock.cookieState = function (blockID, setValue) {
	  if(typeof Drupal.Collapsiblock.cookies == 'undefined') {
      // memoize the cookies, so we only have to load them once
	    if ($.cookie) {
	      var cookieString = $.cookie('collapsiblock');
	      if(cookieString) {
	        Drupal.Collapsiblock.cookies = $.parseJSON(cookieString);
	      }
	      else {
	        Drupal.Collapsiblock.cookies = {};
	      }
	    }
	    else {
	      Drupal.Collapsiblock.cookies = {};
	    }
	  }

    if(typeof(setValue) != 'undefined') {
      Drupal.Collapsiblock.cookies[blockID] = setValue;
      var cookieStrJSON = '{';
      var cookieParts = [];
      $.each(Drupal.Collapsiblock.cookies, function (key, value) {
        cookieParts[cookieParts.length] = ' "' + key + '": ' + value;
        });

      cookieStrJSON += cookieParts.join(', ') + '}';

      $.cookie('collapsiblock', cookieStrJSON, {
        path: Drupal.settings.basePath
        });
    }
    return (blockID in Drupal.Collapsiblock.cookies) ? Drupal.Collapsiblock.cookies[blockID] : 0;
  };

  Drupal.behaviors.collapsiblock = {

    attach: function (context,settings) {
      //var cookieData = Drupal.Collapsiblock.getCookieData();
      var slidetype = settings.collapsiblock.slide_type;
      //var defaultState = settings.collapsiblock.default_state;
      var slidespeed = parseInt(settings.collapsiblock.slide_speed);
      var title = 'h2.block-title'; //settings.collapsiblock.block_title;
      var block = settings.collapsiblock.block;
      var block_content = settings.collapsiblock.block_content;

      $('.collapsiblock').each(function() {
        var titleElt = $(title, this);

        //.not($('.content :header',this));
        if (titleElt.size()) {
          // Cache important classes for use
          titleElt.block_content = $(this).find(block_content);
          titleElt.block = $(this);

          if($(titleElt.block).hasClass('collapsiblock-cookie')) {
            var id = $(titleElt.block).attr('id').replace(/_/g, '-');
            var is_expanded = $(titleElt.block).hasClass('collapsiblock-collapsed') ? 0 : 1;
            // CookieState will return 0 if collapsed 1 if expanded
            if(is_expanded != Drupal.Collapsiblock.cookieState(id)) {
              $(titleElt.block).toggleClass('collapsiblock-collapsed');
            }
          }

          $(titleElt)
          .wrapInner('<a href="#" role="link" />')
          .click(function(e) {
            e.preventDefault();
            
            var is_collapsed = $(titleElt.block).hasClass('collapsiblock-collapsed') ? 1 : 0;
            var id = $(titleElt.block).attr('id').replace(/_/g, '-');

            if(slidetype == 2) {
              // Fade & Slide

              var action = 'hide';
              if(is_collapsed) {
                action = 'show';
              }

              $(titleElt.block_content).animate({
                height: action,
                opacity: action
                }, slidespeed, function() {
                  $(titleElt.block).toggleClass('collapsiblock-collapsed');
              });
            }
            else {
              // Slide
	            $(titleElt.block_content).slideToggle(slidespeed, 'swing', function() { 
                $(titleElt.block).toggleClass('collapsiblock-collapsed');
	              });
            }
            if($(titleElt.block).hasClass('collapsiblock-cookie')) {
              var state = Drupal.Collapsiblock.cookieState(id, is_collapsed);
            }
          });
        }});
    }
  }
})(jQuery);
