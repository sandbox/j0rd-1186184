<?php 

/**
 * Admin settings.
 */
function collapsiblock_admin_settings($form, &$form_state) {
  $form = array();
  $form['collapsiblock_default_state'] = array(
    '#type' => 'radios',
    '#title' => t('Default block collapse behavior'),
    '#options' => array(COLLAPSIBLOCK_NOTHX => t('None.'), COLLAPSIBLOCK_COOKIE_EXPAND => t('Collapsible, expanded by default.'), COLLAPSIBLOCK_COOKIE_COLLAPSE => t('Collapsible, collapsed by default.'), COLLAPSIBLOCK_COLLAPSE => t('Collapsible, collapsed all the time.')),
    '#default_value' => variable_get('collapsiblock_default_state', 1),
  );
  $form['collapsiblock_slide_type'] = array(
    '#type' => 'radios',
    '#title' => t('Default animation type'),
    '#options' => array(1 => t('Slide'), 2 => t('Fade and slide')),
    '#description' => t('Slide is the Drupal default while Fade and slide adds a nice fade effect.'),
    '#default_value' => variable_get('collapsiblock_slide_type', 1),
  );
  $form['collapsiblock_slide_speed'] = array(
    '#type' => 'select',
    '#title' => t('Animation speed'),
    '#options' => drupal_map_assoc(array('50', '100', '200', '300', '400', '500', '700', '1000', '1300')),
    '#description' => t('The animation speed in milliseconds.'),
    '#default_value' => variable_get('collapsiblock_slide_speed', 200),
  );
  $form['collapsiblock_help'] = array(
    '#markup' => t('If collapsiblock doesn\'t work out of the box, you can force CSS selectors on <a href="!url">appearance settings</a>.', array('!url' => url('admin/appearance/settings'))),
  );
  
  $form = system_settings_form($form);
  return $form;
}

